<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setUsername('user_one')
            ->setEmail('user_one@mail.ru')
            ->setEnabled('1')
            ->setPlainPassword('123')
            ->setRoles(['ROLE_USER']);

        $manager->persist($user);

        $user = new User();
        $user
            ->setUsername('user_two')
            ->setEmail('user_two@mail.ru')
            ->setEnabled('2')
            ->setPlainPassword('321')
            ->setRoles(['ROLE_USER']);

        $manager->persist($user);
        $manager->flush();
    }
}
