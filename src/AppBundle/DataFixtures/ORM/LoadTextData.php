<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Text;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTextData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $text = new Text();
        $text->translate('ru')->setText('Привет Друг');
        $text->translate('en')->setText('Hello Friend');
        $text->translate('fr')->setText('Bonjour mon ami');
        $text->translate('de')->setText('Hallo Freund');
        $text->translate('kg')->setText('Салам дос');
        $text->translate('it')->setText('Ciao amico');

        $manager->persist($text);
        $text->mergeNewTranslations();

        $text = new Text();
        $text->translate('ru')->setText('Мы учим языки программирования');
        $text->translate('en')->setText('We learn programming languages');
        $text->translate('fr')->setText('Nous enseignons les langages de programmation');
        $text->translate('de')->setText('Wir unterrichten Programmiersprachen');
        $text->translate('kg')->setText('Биз программалоо тилдерин үйрөтүү');
        $text->translate('it')->setText('Insegniamo linguaggi di programmazione');

        $manager->persist($text);
        $text->mergeNewTranslations();

        $manager->flush();
    }
}
