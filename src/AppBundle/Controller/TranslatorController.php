<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Text;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TranslatorController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction(Request $request)
    {
        $isUserHere = $this->getUser();

        $interfaceTextWelcome = $this->get('translator')->trans('Welcome to the Translation Shop', [], 'interface');
        $interfaceTextAboutUs = $this->get('translator')->trans('About Us', [], 'interface');
        $interfaceTextOrderList = $this->get('translator')->trans('Order list', [], 'interface');
        $interfaceTextAddText = $this->get('translator')->trans('Add text for translation', [], 'interface');
        $interfaceTextLogout = $this->get('translator')->trans('Logout', [], 'interface');

        $formTextRu = $this->get('translator')->trans('Enter text in Russian');
        $formTextButton = $this->get('translator')->trans('Save');


        $form = $this->createFormBuilder()
            ->add('ruText', TextType::class, ['label' => $formTextRu])
            ->add('save', SubmitType::class, ['label' => $formTextButton])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $content = new Text();
            $content->translate('ru')->setText($data['ruText']);


            $em = $this->getDoctrine()->getManager();
            $em->persist($content);

            $content->mergeNewTranslations();
            $em->flush();
            return $this->redirectToRoute("app_translator_index");
        }

        $allContents = $this->getDoctrine()->getRepository('AppBundle:Text')->findAll();

        return $this->render('@App/translator/index.html.twig', array(
                'isUser' => $isUserHere,
                'form' => $form->createView(),
                'contents' => $allContents,
                'welcome' => $interfaceTextWelcome,
                'aboutUs' => $interfaceTextAboutUs,
                'orderList' => $interfaceTextOrderList,
                'addText' => $interfaceTextAddText,
                'logout' => $interfaceTextLogout,
        ));
    }

    /**
     * @Route("/translateText/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "HEAD", "POST"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function translateTextAction(Request $request, Text $id)
    {
        $interfaceTextTranslatedText = $this->get('translator')->trans('Translated text', [], 'interface');
        $interfaceTextOriginalText = $this->get('translator')->trans('Original text', [], 'interface');
        $interfaceTextLogout = $this->get('translator')->trans('Logout', [], 'interface');

        $formTextEn = $this->get('translator')->trans('Enter text in English');
        $formTextFr = $this->get('translator')->trans('Enter text in French');
        $formTextDe = $this->get('translator')->trans('Enter text in German');
        $formTextKg = $this->get('translator')->trans('Enter text in Kyrgyz');
        $formTextIt = $this->get('translator')->trans('Enter text in Italian');
        $formTextButton = $this->get('translator')->trans('Save');

        $textForTranslate =$this
            ->getDoctrine()
            ->getRepository('AppBundle:Text')
            ->find($id);

        $form = $this->createFormBuilder()
            ->add('enText', TextType::class, ['label' => $formTextEn, 'required' => false])
            ->add('frText', TextType::class, ['label' => $formTextFr, 'required' => false])
            ->add('deText', TextType::class, ['label' => $formTextDe, 'required' => false])
            ->add('kgText', TextType::class, ['label' => $formTextKg, 'required' => false])
            ->add('itText', TextType::class, ['label' => $formTextIt, 'required' => false])
            ->add('save', SubmitType::class, ['label' => $formTextButton])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $textForTranslate->translate('en')->setText($data['enText']);
            $textForTranslate->translate('fr')->setText($data['frText']);
            $textForTranslate->translate('de')->setText($data['deText']);
            $textForTranslate->translate('kg')->setText($data['kgText']);
            $textForTranslate->translate('it')->setText($data['itText']);


            $em = $this->getDoctrine()->getManager();
            $em->persist($textForTranslate);

            $textForTranslate->mergeNewTranslations();
            $em->flush();
            return $this->redirectToRoute("app_translator_index");
        }
        $isUserHere = $this->getUser();
        return $this->render('@App/translator/translate_text.html.twig', array(
            'text' => $textForTranslate,
            'formForTranslate' => $form->createView(),
            'textTranslated' => $interfaceTextTranslatedText,
            'originalText' => $interfaceTextOriginalText,
            'logout' => $interfaceTextLogout,
            'isUser' => $isUserHere,
        ));
    }

    /**
     * @Route("/{_locale}/", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changeLanguageAction(Request $request){
        return $this->redirect(
            $request
                ->headers
                ->get('referer')
        );
    }
}
